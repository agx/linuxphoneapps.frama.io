+++
title = "Submit apps via GitLab"
description = "Adding or editing existing listings"
date = 2022-04-07T19:30:00+02:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

### Introduction

To edit or add an app, you will have to edit a markdown file with extensive TOML frontmatter, which then is going to be rendered by [Zola](https://getzola.org).


All content regarding apps or games lives in the [LinuxPhoneApps repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io) in content/apps or content/games/, depending on what you want to edit.

### Edit an app listing 

Just click on the "edit this page on framagit" link at the end of the app listing, and click "Open in Web IDE".

Now you'll be asked to login into framagit. You can use your existing GitHub, Gitlab.com or Bitbucket account to speed up that procedure, if you want to. Afterwards, the repo will be forked and you can edit the file locally in GitLab.

Check [Listings explained](@/docs/contributing/listings-explained.md) for reference. Afterwards, create a Merge Request to get your changes merged!

### Adding an app

To add an app, just download the template file for [apps](https://linuxphoneapps.org/new-app.md) or [games](https://linuxphoneapps.org/new-game.md), and fill in at least the mandatory fields according to [Listings explained](@/docs/contributing/listings-explained.md) for reference. Save the file with the apps lower case App ID (e.g., `org.gnome.ghex.md`), if it does not have an App ID, the naming convention is as follows: noappid.authornickname.appname.md

If you want help out, but have not found an app to add, help working through [our To Do list](https://linuxphoneapps.org/lists/apps-to-be-added/)!

After that, fork the repo and add the file into content/apps/ or content/games/ via file upload or locally. 

Then create a merge request!

<mark>TBD: Explanation not ready yet, missing: How to create a merge request - and maybe more - please contribute to make this better!</mark>


