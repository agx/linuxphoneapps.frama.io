+++
title = "Mobian"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

[Mobian](https://mobian.org) is currently based on [Debian 12 "Bookworm"](../debian-12/) and is going to stabilize on it, while also having a "testing branch" based on the next Debian testing, "Trixie". There's also a related project called [Droidian](https://droidian.org/), that makes Mobian run on Android phones that are not supported by a mainline kernel.

Please note: [Mobian](https://mobian.org) also have an additional repository with custom packages, which is not and will not be indexed on [repology.org](https://repology.org) (see [github issue](https://github.com/repology/repology-updater/issues/1291), so not every package available to you on Mobian can be listed here.

See also: [Mobian Wiki: Apps](https://wiki.mobian.org/doku.php?id=apps), [Mobian Wiki: Wishlist](https://wiki.mobian.org/doku.php?id=wishlist)

