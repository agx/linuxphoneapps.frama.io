+++
title = "Kairo"
description = "Programmable timer"
aliases = []
date = 2021-02-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL",]
metadata_licenses = []
app_author = [ "utilities",]
categories = [ "timer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "Clock",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/kairo"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/kairo/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kairo"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kairo"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kairo",]
appstream_xml_url = "https://invent.kde.org/utilities/kairo/-/raw/master/org.kde.kairo.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++



