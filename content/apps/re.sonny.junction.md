+++
title = "Junction"
description = "Application chooser"
aliases = []
date = 2023-06-25

[taxonomies]
project_licenses = [ "GPL-3.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sonny Piers",]
categories = [ "Utility",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = []
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://junction.sonny.re/source"
homepage = "https://junction.sonny.re"
bugtracker = "https://junction.sonny.re/feedback"
donations = "https://junction.sonny.re/donate"
translations = "https://junction.sonny.re/translate"
more_information = []
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/sonnyp/Junction/main/data/screenshot.png", "https://raw.githubusercontent.com/sonnyp/Junction/main/data/screenshot-file.png", "https://raw.githubusercontent.com/sonnyp/Junction/main/data/screenshot-shortcuts.png", "https://raw.githubusercontent.com/sonnyp/Junction/main/data/screenshot-desktop-actions.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/re.sonny.junction/1.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "re.sonny.Junction"
scale_to_fit = ""
flathub = "https://flathub.org/apps/re.sonny.Junction"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "junction-application-browser",]
appstream_xml_url = "https://raw.githubusercontent.com/sonnyp/Junction/main/data/re.sonny.Junction.metainfo.xml"
reported_by = "linmob"
updated_by = ""
+++



### Description

Junction lets you choose the application to open files and links.
After installing make sure to launch the application.
Junction will pop up automatically when you open a link in a desktop application.
Use the mouse or keyboard navigation to choose the application to open the link or file with.

Features:
* Choose the application to open with
* Show the location before opening
* Edit the URL before opening
* Show a hint for insecure link
* Keyboard navigation
* Middle click to open in multiple applications

[Source](https://raw.githubusercontent.com/sonnyp/Junction/main/data/re.sonny.Junction.metainfo.xml)

