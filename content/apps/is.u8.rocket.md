+++
title = "Rocket"
description = "GTK Gemini browser"
aliases = []
date = 2021-05-16

[taxonomies]
project_licenses = [ "EUPL-1.2",]
metadata_licenses = []
app_author = [ "alva",]
categories = [ "gemini browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "Zig",]
build_systems = [ "zig",]

[extra]
repository = "https://git.sr.ht/~alva/rocket"
homepage = "https://sr.ht/~alva/rocket/"
bugtracker = "https://todo.sr.ht/~alva/rocket"
donations = ""
translations = ""
more_information = [ "https://todo.sr.ht/~alva/rocket?search=status%3Aopen%20label%3A%22feature%22",]
summary_source_url = "https://git.sr.ht/~alva/rocket"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1393885172228136962",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "is.u8.rocket"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = ""

+++


### Description

Rocket is a Gemini browser. Somewhat usable, albeit with missing or incomplete features. [Source](https://git.sr.ht/~alva/rocket)


### Notice

WIP, scales nicely.