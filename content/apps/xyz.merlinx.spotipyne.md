+++
title = "Spotipyne"
description = "Gtk Spotify client written in python made to be compatible with mobile formfactors like a pinephone."
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "dann-merlin",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Spotify",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/dann-merlin/spotipyne"
homepage = ""
bugtracker = "https://gitlab.com/dann-merlin/spotipyne/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/dann-merlin/spotipyne"
screenshots = [ "https://gitlab.com/dann-merlin/spotipyne",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "xyz.merlinx.Spotipyne"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/dann-merlin/spotipyne/-/raw/master/data/xyz.merlinx.Spotipyne.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++