+++
title = "MaxControl"
description = "Control software for Max!Cube family devices"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Jan-Michael Brummer",]
categories = [ "smart home",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/tabos/maxcontrol"
homepage = "https://tabos.gitlab.io/projects/maxcontrol/"
bugtracker = "https://gitlab.com/tabos/maxcontrol/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/tabos/maxcontrol"
screenshots = [ "https://tabos.gitlab.io/projects/maxcontrol/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.tabos.maxcontrol"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.maxcontrol"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "max-control",]
appstream_xml_url = "https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++







### Notice

Testing requires a Max!Cube device, help needed.
