+++
title = "Hermes Messenger"
description = "Unofficial Fb Messenger app for linux smartphone"
aliases = []
date = 2021-05-08
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "mrbn100ful",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Electron",]
backends = []
services = [ "Facebook Messenger", "Messenger.com",]
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "InstantMessaging", "Chat",]
programming_languages = [ "Javascript",]
build_systems = [ "yarn",]

[extra]
repository = "https://github.com/MrBn100ful/HermesMessenger"
homepage = ""
bugtracker = "https://github.com/MrBn100ful/HermesMessenger/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/MrBn100ful/HermesMessenger"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.mrbn100ful.hermesmessenger"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/MrBn100ful/HermesMessenger/master/data/com.github.mrbn100ful.hermesmessenger.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Hermes Messenger is a unofficial Gtk WebKit Facebook messenger app made for linux smartphone (eg: librem 5, pinephone). It's based on the Web version of Fb messenger with some hacks to run properly on a smartphone screen. [Source](https://github.com/MrBn100ful/HermesMessenger)


### Notice

Rewrite to GTK WebKit is in progress, check releases to download prebuilt packages.