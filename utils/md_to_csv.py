#!/usr/bin/env python3

from common import *
from md_parser import MDParser

import argparse
from dataclass_csv import DataclassWriter
import os


apps = []


def parse(input_dir):
    parser = MDParser()
    files = [f for f in os.listdir(input_dir) if os.path.isfile(os.path.join(input_dir, f))]
    for filename in files:
        if filename.endswith('.md') and not filename == '_index.md':
            print("parsing " + filename)
            with open(os.path.join(input_dir, filename), 'r') as f:
                app = parser.load(f)
                if not app.validate():
                    exit(1)
                apps.append(app)


def main():
    global apps

    parser = argparse.ArgumentParser(description='Convert directory of app *.md files to csv.')
    parser.add_argument('input_dir', help='input directory')
    parser.add_argument('--output', help='output filename (default: apps.csv)', default='apps.csv')

    args = parser.parse_args()

    parse(args.input_dir)

    with open(args.output, 'w') as f:
        w = DataclassWriter(f, apps, App)
        w.write()


if __name__ == "__main__":
    main()
