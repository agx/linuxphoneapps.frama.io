#!/usr/bin/python3

import asyncio
import datetime
import pathlib
import re
import sys
import traceback

import aiofiles
import appstream_python
import frontmatter
import httpx
import markdownify

import utils


async def load_appstream(client, url):
    if not url:
        return None
    app = appstream_python.AppstreamComponent()
    try:
        response = await client.get(url)
        if response.status_code != httpx.codes.OK:
            print(f"Error loading {url}", file=sys.stderr)
            return None
        app.load_bytes(response.content, encoding=response.encoding)
    except Exception as e:
        print(f"Error loading {url}:", file=sys.stderr)
        traceback.print_exception(e, file=sys.stderr)
        return None
    return app



def get_appstream_description(app):
    return markdownify.markdownify(app.description.to_html(lang=None), heading_style="ATX")



async def check(client, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")
    app = await load_appstream(client, utils.get_recursive(item, "extra.appstream_xml_url"))
    if not app:
        return False

    properties = [
        {"apps_key": "description_md", "handler": get_appstream_description},
    ]
    found = False
    for property in properties:
        try:
            found_entry = property["handler"](app)
        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_key"]}:', file=sys.stderr)
            traceback.print_exception(e, file=sys.stderr)
            continue

        if utils.get_recursive(item, "extra.appstream_xml_url").endswith(".in") and isinstance(found_entry, str):
            if re.search(r"@.*@", found_entry):
                print(f'{item_name}: Ignoring autotools template variable for {property["apps_key"]}: {found_entry}', file=sys.stderr)
                continue

        if utils.get_recursive(item, property["apps_key"]) and not found_entry:
            print(f'{item_name}: {property["apps_key"]} missing in upstream AppStream file. Consider contributing it upstream: {utils.get_recursive(item, property["apps_key"])}', file=sys.stderr)
        if not found_entry or found_entry == utils.get_recursive(item, property["apps_key"]):
            continue  # already up to date

        message = f'{item_name}: {property["apps_key"]} '
        if not utils.get_recursive(item, property["apps_key"]):
            message += "new: "
        else:
            message += f'outdated "{utils.get_recursive(item, property["apps_key"])}" -> '
        message += f'"{found_entry}"'
        print(message, file=sys.stderr)

        found = True
        if update:
            utils.set_recursive(item, property["apps_key"], found_entry)
            if utils.get_recursive(item, source_column := (property["apps_key"] + "_source")):
                if utils.get_recursive(item, source_column) != utils.get_recursive(item, "extra.appstream_xml_url"):
                    print(f'{item_name}: {source_column} {utils.get_recursive(item, source_column)} -> {utils.get_recursive(item, "extra.appstream_xml_url")}', file=sys.stderr)
                utils.set_recursive(item, source_column, utils.get_recursive(item, "extra.appstream_xml_url"))

    return found

async def check_file(client, filename, update=False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        content = await f.read()

    doc = frontmatter.loads(content)
    found = await check(client, doc.metadata, update)

    if found and update:
        description = utils.get_recursive(doc.metadata, "description_md")
        source_url = utils.get_recursive(doc.metadata, "extra.appstream_xml_url")
        if description:
            # Replace existing description with new description and source URL
            content = re.sub(r"(### Description\n)(.*?)(\n### Notice|$)",
                             rf"\1{description}\n[Source]({source_url})\n\3", 
                             content, flags=re.DOTALL)
        else:
            # Add new description and source URL
            content += f"\n### Description\n{description}\n\n\n[Source]({source_url})\n\n"

        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(content)

    return found


async def run(folder, update=False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        for filename in folder.glob("**/*.md"):
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FOLDER")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_folder = pathlib.Path(sys.argv[2])
    found = await run(apps_folder, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_folder}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
